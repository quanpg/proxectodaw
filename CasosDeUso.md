# Usuario:

    ## Gestion de alimentos

    - Crear nuevo alimento
        - Escribir datos del alimento
            - Energía
            - Proteína
            - Hidratos de carbono
            - Fibra
            - Grasa
                - Saturada
                - Monoinsaturada
                - Polinsaturada
                - Colesterol
            - Colesterol
            - Alcohol
            - Agua
            - Minerales
                - Calcio
                - Hierro
                - Yodo
                - Magnesio
                - Zinc
                - Selenio
                - Sodio
                - Potasio
                - Fósforo
            - Vitaminas
                - B1 Tiamina
                - B2 Riboflavina
                - Niacina
                - B6 Piridoxina
                - Ácido fólico

                - B12 Cianocobalamina
                - C Ácido ascórbico
                - Retinol
                - Carotenoides
                - A
                - D

        - Validar campos
        - Guardar en la base de datos

    - Listar Alimentos
        - Consultar base de datos
        - Mostrar resultados

    - Buscar alimento
        - Introducir nombre de alimento
        - Buscar coincidencias en la base de datos
        - Mostrar resultados

    - Ver informacion nutricional de un alimento 
        - Listar alimentos / Buscar alimento
        - Seleccionar alimento
        - Consultar base de datos
        - Mostrar resultados 

    - Editar alimento
        - Buscar alimento a editar
        - Modificar datos del alimento
        - Validar datos del alimento
        - Guardar en la base de datos

    - Eliminar alimento 
        - Buscar alimento
        - Eliminar alimento de la base de datos


    ## Gestion de diario

    - Agregar alimento a diario
        - Buscar Alimento / Crear nuevo alimento
        - Indicar cantidad a introducir
        - Guardar en la base de datos

    - Editar cantidad de un alimento en diario
        - Mostrar alimentos en el diario
        - Seleccionar alimento del diario
        - Editar cantidad de alimento
        - Guardar en base de datos

    - Eliminar alimentos en diario
        - Mostrar alimentos en el diario
        - Seleccionar elemento a eliminar
        - Eliminar de la base de datos

    - Visualizar diarios
        - seleccionar dia
        - consultar y mostrar datos del dia seleccionado


    ## Gestion de peso

    - Agregar peso diario
        - Escribir peso 
        - Guardar en la base de datos

    - Consultar historico de pesos

    - Eliminar peso diario
        - Seleccionar peso a eliminar
        - Eliminar de la base de datos  


    ## Gestion de usuarios

    - Registro 
        - Mostrar formulario de registro
        - Introducir datos del registro
            - Nombre real
            - Login
            - Correo electrónico
            - Peso
            - Altura
            - Fecha nacimiento
            - Biotipo
            - Actividad física

        - Validar datos
        - Guardar en la base de datos

    - Login
        - Mostrar formulario 
        - Introducir usuario y contraseña
        - Comprobar credenciales
        - Conceder o denegar acceso 

    - Agregar/Editar perfil de usuario
        - Introducir datos del perfil
        - Validar datos
        - Recalcular requisitos si fuese necesario
        - Guardar en base de datos

    ## Gestion de comidas

    - Crear comida
        - Introducir nombre de la comida
        - Introducir alimentos y cantidades de la comida
        - Validar datos
        - Guardar en la base de datos

    - Editar comida
        - Seleccionar la comida a editar
        - Modificar alimentos y/o cantidades
        - Validar datos
        - Guardar en la base de datos

    - Eliminar comida 
        - Seleccionar comida a eliminar
        - Eliminar de la base de datos


    ## Gestion agua
    - Agregar/Eliminar vaso de agua
